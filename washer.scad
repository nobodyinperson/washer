/* [🔩 Washer] */
washer_outer_diameter = 30; // [1:0.1:100]
washer_hole_diameter = 10;  // [1:0.1:100]
washer_thickness = 3;       // [0.1:0.1:50]

/* [Precision] */
$fs = $preview ? 2 : 0.5;
$fa = $preview ? 5 : 1;
epsilon = 0.1 * $fs;

module
washer(outer_diameter = washer_outer_diameter,
       hole_diameter = washer_hole_diameter,
       thickness = washer_thickness)
{
  difference()
  {
    cylinder(d = outer_diameter, h = thickness);
    translate([ 0, 0, -epsilon / 2 ])
      cylinder(d = hole_diameter, h = thickness + epsilon);
  }
}

washer(outer_diameter = washer_outer_diameter,
       hole_diameter = washer_hole_diameter,
       thickness = washer_thickness);
